# PTG

El ejemplo que tenemos funcionando es la prueba del intérprete
heterogéneo, el cuál utiliza al intérprete geométrico.

Se tienen tres archivos


La definición de objetos geométricos en `PTG/het_reasoning/test_eval/example1/g_objects`:

```prolog
%% dots
g_def(d1,dot(true,0.2:0.2)).
g_def(d2,dot(true,0.7:0.2)).
g_def(d3,dot(true,0.7:0.7)).
g_def(d4,dot(true,0.2:0.7)).
g_def(d5,dot(true,0.4:0.4)).
g_def(d6,dot(true,1.0:0.4)).
g_def(d7,dot(true,1.0:1.0)).
g_def(d8,dot(true,0.4:1.0)).
g_def(d9,dot(true,0.5:0.5)).
g_def(d10,dot(true,0.6:0.5)).
g_def(d11,dot(true,0.55:0.6)).
g_def(d12,dot(true,0.8:0.8)).
g_def(d13,dot(true,0.9:0.8)).
g_def(d14,dot(true,0.85:0.9)).

%% triang
g_def(t1,triang(_,d9,d10,d11)).
g_def(t2,triang(_,d12,d13,d14)).

%% square
g_def(sq1,square(_,d1,d2,d3,d4)).
g_def(sq2,square(_,d5,d6,d7,d8)).
```

La definición de representaciones en `PTG/het_reasoning/test_eval/example1/represent`:

```prolog
%% figuras geometricas representan individuos
rep(juan,t1).
rep(pedro,t2).
rep(linguistica,sq1).
rep(programacion,sq2).

%% funciones geometricas representan preposiciones
rep(en,lambda([X,Y],in(true,X,Y))).
```

La definición de la teoría en `PTG/het_reasoning/test_eval/example1/theory`:

```prolog
estudiante(juan).
estudiante(pedro).
materia(programacion).
materia(linguistica).
estudia(X, Y) :- estudiante(X), materia(Y), en(X, Y).
listo(X) :- estudia(X, linguistica), estudia(X, programacion).
```

La teoría es como un programa de prolog común y corriente, sin
embargo, no hay una definición del predicado `en` disponible.

Sin embargo, hay una representación para `en` y es
`lambda([X,Y],in(true,X,Y))` donde `in` es un procedimiento geométrico
para determinar si una figura está traslapada con otra.

Se utilizan las representaciones geométricas de `juan`, `pedro`,
`linguistica` y `programacion` para poder efectuar la inferencia.

Para correr esta prueba, ejecuta desde la línea de comandos, en la
raíz del directorio de este repositorio:

```
$ swipl demo.pl
```

Aparecerá un prompt que te pide escribir un término.

Podemos ahora preguntarnos si `pedro` es `listo`:

```
'input termino a evaluar '
|: listo(pedro).

'No hay soluciones '
'SOlS: '[]
'otra pregunta heterogena? '|:
```

También podemos pedir por la relación `estudia(X, Y)`:

```
...
'otra pregunta heterogena? '|: yes.
'input termino a evaluar '
|: estudia(X, Y).

'SOLUCIONES: '[estudia(juan,linguistica),estudia(juan,programacion),estudia(pedro,programacion)]
'otra pregunta heterogena? '|: no.
```

Estas consultas efectúan las inferencias trasladando conocimiento
entre el mundo simbólico de Prolog y el mundo geométrico.
