%code for the 5 parameters values for vector intersections
%select operation term according to parameters
%			1: T < 0,
%			2: T = 0,
%			3: 0 < T < 1,
%			4: T = 1,
%			5: T > 1.
code_parameter(T, 1) :- error(T,TE), TE < 0.
code_parameter(T, 2) :- error(T,TE), TE == 0.
code_parameter(T, 3) :- error(T,TE), 0 < TE, TE < 1.
code_parameter(T, 4) :- error(T,TE), TE == 1.
code_parameter(T, 5) :- error(T,TE), TE > 1.

t_case(T1, T2) :- 
			(T1 == 3, T2 == 2);
			(T1 == 3, T2 == 4);
			(T2 == 3, T1 == 2);
			(T2 == 3, T1 == 4),
			!.
e_case(T1, T2) :-	
			(T1 == 2, T2 == 2);
			(T1 == 2, T2 == 4);
			(T1 == 4, T2 == 2);
			(T1 == 4, T2 == 4),
			!.
			
%table of intersection operation according to parameter pair
op_name(2,2,int_oo).
op_name(2,3,int_om).
op_name(2,4,int_oe).
op_name(3,2,int_mo).
op_name(3,3,int_mm).
op_name(3,4,int_me).
op_name(4,2,int_eo).
op_name(4,3,int_em).
op_name(4,4,int_ee).
op_name(_,_,int_ww).

c_op_name(C1,C2,e_join_at) :- e_case(C1, C2).
c_op_name(C1,C2,t_join_at) :- t_case(C1, C2).
c_op_name(3,3,cross_at).
c_op_name(_,_,intersect_at).


%Find point given a vector and a parameter from its origin.
intersection_point([X0:Y0,X1:Y1], T, X:Y) :-
		DX is X1 - X0,
		DY is Y1 - Y0,
		XT is T * DX,
		YT is T * DY,
		X is X0 + XT,
		Y is Y0 + YT,
		!.

%test whether a dot is on the infinite projection of a line
on_projection(true, X:Y, [X0:Y0,X1:Y1]) :- 
		determinant([X0:Y0,X:Y],[X1:Y1,X:Y],D,T1,T2),
		error(D,0),
		error(T1,0),
		error(T2,0),
		!.
on_projection(false, _, _).

medium_point(X1:Y1, X2:Y2, X:Y) :-
			XD is X1 + X2,
			YD is Y1 + Y2,
			X is XD/2,
			Y is YD/2,
			!.

distance(X1:Y1, X2:Y2, D) :-
		DX is X2 - X1,
		DY is Y2 - Y1,
		DX2 is DX*DX,
		DY2 is DY*DY,
		DIF is DX2 + DY2,
		D is sqrt(DIF),
		!.

determinant([X0:Y0,X1:Y1],[X2:Y2,X3:Y3],DETER, DT1, DT2) :-
		DX10 is X1 - X0,
		DX32 is X3 - X2,
		DY10 is Y1 - Y0,
		DY32 is Y3 - Y2,
		PDET1 is DX10 * DY32,
		PDET2 is DX32 * DY10,
		DETER is PDET2 - PDET1,

		%first discriminant
		DX20 is X2 - X0,
		DY20 is Y2 - Y0,
		PDISC11 is DX20 * DY32,
		PDISC12 is DX32 * DY20,
		DT1 is PDISC12 - PDISC11,

		%second discriminant
		PDISC21 is DX10 * DY20,
		PDISC22 is DX20 * DY10,
		DT2 is PDISC21 - PDISC22,
		!.

%test for coincidence between two extreme dots of two different vectors
extreme_point([D1,_],[D2,_],D) :- match_points(true,D1,D2),
				  medium_point(D1,D2,D).
extreme_point([D1,_],[_,D2],D) :- match_points(true,D1,D2),
				  medium_point(D1,D2,D).
extreme_point([_,D1],[D2,_],D) :- match_points(true,D1,D2),
				  medium_point(D1,D2,D).
extreme_point([_,D1],[_,D2],D) :- match_points(true,D1,D2),
				  medium_point(D1,D2,D).

dot_in_box(X:Y,[X1:Y1,X2:Y2]) :-
			max(X1,X2,XMAX),
			max(Y1,Y2,YMAX),
			min(X1,X2,XMIN),
			min(Y1,Y2,YMIN),
			X =< XMAX,
			X >= XMIN,
			Y =< YMAX,
			Y >= YMIN.

match_points(true,D1,D2) :-
			distance(D1,D2,D),
			delta_error(ERROR),
			MARGIN is 4 * ERROR,
			D < MARGIN,
			!.
match_points(false,_,_).

%regresa la lista de vectores que conforman a una fig. dada su def. parametrica
vectors_fig(FIG,VECTORS) :-
			FIG =.. [F|[true,D1,D2,D3]],
			(F = right_triang ; F = triang),
			make_vectors([D1,D2,D3,D1],VECTORS).

vectors_fig(FIG,VECTORS) :-
			FIG =.. [F|[true,D1,D2,D3,D4]],
			(F = square ; F = rectangle),
			make_vectors([D1,D2,D3,D4,D1],VECTORS).

vectors_fig(polygon(true,DOTS),VECTORS) :-
			first(DOTS,FIRST),
			reverse(DOTS,RDOTS),
			append([FIRST],RDOTS,CDOTS),
			reverse(CDOTS,LIST_DOTS),
			make_vectors(LIST_DOTS,VECTORS).

make_vectors([_|[]],[]).
make_vectors([dot(true,X1:Y1)|T],VECTORS) :-
			first(T,dot(true,X2:Y2)),
			make_vectors(T,VECTS),
			append([[X1:Y1,X2:Y2]],VECTS,VECTORS).

		


