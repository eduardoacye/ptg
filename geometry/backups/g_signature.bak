%Def. de la signatura
%
%Sorts: 
%	int
%	real
%	dot 
%	line
%	path
%	triang
%	right_triang
%	square
%	rectangle
%	polygon
%
%Sorts adicionales:
%      square_pair
%      corner_frame
%
%Sort de funciones:
%	func
%
%Orden (Se implementa en la sem. de los predicados pertinentes)
%	line < path
%	right_triang < triang < polygon 
%	square < rectangle < polygon
%	parallelogram < polygon
%	corner_frame < polygon
%
%Constructores geometricos:
%	dot: bool x real:real --> dot 
%	line: bool x dot x dot --> line
%	path: bool x dots_list --> path
%	right_triang: bool x dot x dot x dot --> right_triang
%	triang: bool x dot x dot x dot --> triang
%	square: bool x dot x dot x dot x dot --> square
%	rectangle: bool x dot x dot x dot x dot --> rectangle
%	parallelogram: bool x dot x dot x dot x dot --> paralelogram
%	polygon: bool x dots_list --> polygon
%
%	Se define ademas los tipos no graficos:
%		Booleano: bool
%		Real: real
%		Real pair: real_pair

%Tabla de functores y argumentos
%NOTACION: 
%	functor: arg1 x arg2,..., argN --> type 
%
%			 ==> 
%
%	op_g(functor,[arg1,arg2,...,argN],type).

%Constructores
op_g(dot,[bool,real_pair],dot).
op_g(line,[bool,dot,dot],line).
op_g(path,[bool,list_of_dots],path).
op_g(right_triang,[bool,dot,dot,dot],right_triang).
op_g(triang,[bool,dot,dot,dot],triang).
op_g(square,[bool,dot,dot,dot,dot],square).
op_g(rectangle,[bool,dot,dot,dot,dot],rectangle).
op_g(parallelogram,[bool,dot,dot,dot,dot],parallelogram).
op_g(polygon,[bool,list_of_dots],polygon).

%Sorts para probar Pitagagoras
%Sort adicional: square_pair
op_g(square_pair,[bool,square,square],square_pair).
op_g(area,[bool,square_pair],real).
op_g(union,[bool,square,square],square_pair).

%Sorts para el teorema de la suma de los nones
%Sort adicional: corner_frame
op_g(corner_frame,[bool,dot,real],corner_frame).
op_g(pivot,[bool,corner_frame],dot).
op_g(position,[bool,corner_frame],real_pair).
op_g(size,[bool,corner_frame],real).
op_g(area,[bool,corner_frame],real).
op_g(union,[bool,square,corner_frame],square).

%Se definen tambien un conjunto de constantes de cada tipo para "bautizar"
%a los objetos de la base geometrica con el siguiente formato:
%
%		g_def(id,type,lista_de_parametros).
%
%Estas constantes se interpretan como sigue:
%
%		id = type(bool,lista_de_parametros)
%
%
%Las expresion de G se pueden evaluar en relacion a los objetos graficos 
%definido en la base. El conjunto de objetos en la base representan al diagrama
%
%se define la base geometrica como un conjunto de objectos g_db(id,typo,par)

%Constantes logicas
op_g(and,[bool,bool,bool],bool).
op_g(or,[bool,bool,bool],bool).
op_g(if,[bool,bool,bool],bool).
op_g(iff,[bool,bool,bool],bool).
op_g(not,[bool,bool],bool).

%Aritmetica
op_g(add,[bool,real,real],real).
op_g(mult,[bool,real,real],real).
op_g(add_real_pair,[bool,real_pair,real_pair],real_pair).
op_g(dif,[bool,real,real],real).

%SELECTORES

%Predicados de tipos
op_g(is_dot,[bool,dot],bool).
op_g(is_line,[bool,line],bool).
op_g(is_path,[bool,path],bool).
op_g(is_right_triang,[bool,right_triang],bool).
op_g(is_triang,[bool],bool).
op_g(is_square,[bool,square],bool).
op_g(is_rectangle,[bool,rectangle],bool).
op_g(is_parallelogram,[bool,parallelogram],bool).
op_g(is_polygon,[bool,polygon],bool).
op_g(is_square_pair,[bool,square_pair],bool).
op_g(is_corner_frame,[bool,corner_frame],bool).

%Predicados de igualdad geometrica:
op_g(eq,[bool,real,real],bool).
op_g(eq,[bool,real_pair,real_pair],bool).
op_g(eq,[bool,dot,dot],bool).
op_g(eq,[bool,line,line],bool).
op_g(eq,[bool,path,path],bool).
op_g(eq,[bool,triang,triang],bool).
op_g(eq,[bool,right_triang,right_triang],bool).
op_g(eq,[bool,square,square],bool).
op_g(eq,[bool,rectangle,rectangle],bool).
op_g(eq,[bool,parallelogram,parallelogram],bool).
op_g(eq,[bool,square_pair,square_pair],bool).
op_g(eq,[bool,corner_frame,corner_frame],bool).

%SELECTORES GEOMETRICOS

%Para dot:
op_g(position,[bool,dot],real_pair).

%Para line:
op_g(origen,[bool,line],dot).
op_g(end,[bool,line],dot).
op_g(length,[bool,line],real).
op_g(angle,[bool,line],real).

%Para path:
op_g(origen,[bool,path],dot).
op_g(end,[bool,path],dot).
op_g(num_segments,[bool,path],int).
op_g(segment,[bool,path,int],line).
op_g(length,[bool,path],real).

%Para right_triang:
op_g(side_A,[bool,right_triang],line).
op_g(side_B,[bool,right_triang],line).
op_g(hypothenuse,[bool,right_triang],line).
op_g(angle_AH,[bool,right_triang],real).
op_g(angle_BH,[bool,right_triang],real).
op_g(right_vertex,[bool,right_triang],dot).
op_g(vertex_A,[bool,right_triang],dot).		%opuesto a A
op_g(vertex_B,[bool,right_triang],dot).		%opuesto a B
op_g(base,[bool,right_triang],real).
op_g(height,[bool,right_triang],real).
op_g(area,[bool,right_triang],real).

%Predicado: A, B, H
op_g(is_right_triang,[bool,real,real,real],bool).

%Constructor de right triang, dados A, B, H
op_g(right_triang,[bool,real,real,real],right_triang).

%constructor de clase de equivalencia de los triangulos rectos 
%con los mismos lados rectos e hipotenusa
op_g(class_right_triang,[bool,real,real,real],func).

%Para triang:
op_g(side_A,[bool,triang],line).
op_g(side_B,[bool,triang],line).
op_g(side_C,[bool,triang],line).
op_g(vertex_A,[bool,triang],dot).
op_g(vertex_B,[bool,triang],dot).
op_g(vertex_C,[bool,triang],dot).
op_g(angle_A,[bool,triang],line).
op_g(angle_B,[bool,triang],line).
op_g(angle_C,[bool,triang],line).
op_g(base,[bool,triang],real).
op_g(height,[bool,triang],real).
op_gf(area,[bool,triang],real).

%Para square:
op_g(side_A,[bool,square],line).
op_g(side_B,[bool,square],line).
op_g(side_C,[bool,square],line).
op_g(side_D,[bool,square],line).
op_g(size,[bool,square],real).
op_g(area,[bool,square],real).
op_g(position,[bool,square],real_pair).
op_g(bottom_left_vertex,[bool,square],dot).
op_g(top_right_vertex,[bool,square],dot).

%Constructores para square de ref.
%alineado a los ejes en dir. positiva
%En el origen:
op_g(square,[bool,real],square).

%Con referencia a un vertice (Bottom-Left)
op_g(square,[bool,dot,size],square).

%constructor de clase de equivalencia de squares
%con el mismo lado
op_g(class_square,[bool,real],func).


%Para rectangle:
op_g(side_A,[bool,rectangle],line).
op_g(side_B,[bool,rectangle],line).
op_g(side_C,[bool,rectangle],line).
op_g(side_D,[bool,rectangle],line).
op_g(base,[bool,rectangle],real).
op_g(height,[bool,rectangle],real).
op_g(area,[bool,rectangle],real).

%Para parallelogram:
op_g(side_A,[bool,parallelogram],line).
op_g(side_B,[bool,parallelogram],line).
op_g(side_C,[bool,parallelogram],line).
op_g(side_D,[bool,parallelogram],line).
op_g(base,[bool,parallelogram],real).
%pendientes:
op_g(height,[bool,parallelogram],real).
op_g(area,[bool,parallelogram],real).

%Para polygon: (pendiente)
op_g(area,[bool,polygon],real).
op_g(segment,[bool,polygon,int],line).

%PREDICADOS GEOMETRICOS

%Puntos
op_g(up,[bool,dot,dot],bool).
op_g(down,[bool,dot,dot],bool).
op_g(right,[bool,dot,dot],bool).
op_g(left,[bool,dot,dot],bool).

%Punto y fig.
op_g(on,[bool,dot,line],bool). %sobre la linea
op_g(on_project,[bool,dot,line],bool). %sobre la proj. infinita de la linea
op_g(on_path,[bool,dot,path],bool).
op_g(origen,[bool,dot,line],bool).
op_g(origen_path,[bool,dot,path],bool).
op_g(end,[bool,dot,line],bool).
op_g(end_path,[bool,dot,path],bool).
op_g(in,[bool,dot,polygon],bool).

%relaciones entre figuras
op_g(in,[bool,polygon,polygon],bool).
op_g(intersect,[bool,polygon,polygon],bool).

%lineas
op_g(horizontal,[bool,line],bool).
op_g(vertical,[bool,line],bool).
op_g(parallel,[bool,line,line],bool).
op_g(perpendicular,[bool,line,line],bool).
op_g(colineal,[bool,line,line],bool).
op_g(on,[bool,line,line],bool).
op_g(intersect,[bool,line,line],bool).

%lines & square

op_g(side,[bool,line,right_triang],bool).
op_g(side,[bool,line,triang],bool).
op_g(side,[bool,line,square],bool).
op_g(side,[bool,line,rectangle],bool).

%right_triang 
%alineacion por las hipotenusas (exactamente)
op_g(aligned_HH,[bool,right_triang,right_triang],bool).

%alineacion por hipotenusas y un lado recto (exactamente)
op_g(aligned_H_LR,[bool,right_triang,right_triang],bool).

%alineacion por un lado recto y el vertice recto de ambos right_triang
op_g(aligned_AB_CRV,[bool,right_triang,right_triang],bool).

%alineacion por un lado recto y un vertice NO recto de ambos right_triang
op_g(aligned_AB_NCRV,[bool,right_triang,right_triang],bool).
op_g(vertex_aligned_AB_NCRV,[bool,right_triang,right_triang],dot).

%FUNCIONES GEOMETRICAS
%
%Puntos
op_g(distance,[bool,dot,dot],real).
op_g(distance,[bool,dot,line],real).  %la distancia mas corta
op_g(cons_dot,[bool,dot,dot],dot).    %IF D1 = D2, then D1 = D2 = D3

%lineas
op_g(angle,[bool,line,line],real).

%def. line por pto. y ang. en marco unit
op_g(line,[bool,dot,real],line).  

%pto. medio de linea
op_g(midle_dot,[bool,line],dot).
op_g(cross_at,[bool,line,line],dot).
op_g(t_join_at,[bool,line,line],dot).
op_g(e_join_at,[bool,line,line],dot).

%cualquier int. actual o en proyeccion
op_g(intersect_at,[bool,line,line],dot).

%right_triang (figuras construidas por alineacion sin traslape)
op_g(square,[bool,right_triang,right_triang],square).
op_g(rectangle,[bool,right_triang,right_triang],rectangle).



%TRANSFORMACIONES GEOMETRICAS
op_g(translate,[bool,dot,real:real],dot).
op_g(translate,[bool,line,real:real],line).
op_g(translate,[bool,path,real:real],path).
op_g(translate,[bool,right_triang,real:real],right_triang).
op_g(translate,[bool,triang,real:real],triang).
op_g(translate,[bool,square,real:real],square).
op_g(translate,[bool,rectangle,real:real],rectangle).
op_g(translate,[bool,parallelogram,real:real],parallelogram).
op_g(translate,[bool,polygon,real:real],poygon).

%rotacion de un pto. respecto al origen
op_g(rotate,[bool,dot,real],dot).

%rotacion de figuras respecto a un pto. de referencia
op_g(rotate,[bool,dot,real,dot],line).
op_g(rotate,[bool,line,real,dot],line).
op_g(rotate,[bool,path,real,dot],path).
op_g(rotate,[bool,right_triang,real,dot],right_triang).
op_g(rotate,[bool,triang,real,dot],triang).
op_g(rotate,[bool,square,real,dot],square).
op_g(rotate,[bool,rectangle,real,dot],rectangle).
op_g(rotate,[bool,parallelogram,dot],parallelogram).
op_g(rotate,[bool,polygon,real,dot],poygon).

%Simetria focal
op_g(symmetrical,[bool,dot,dot],dot).
op_g(symmetrical,[bool,line,dot],line).
op_g(symmetrical,[bool,path,dot],path).
op_g(symmetrical,[bool,right_triang,dot],right_triang).
op_g(symmetrical,[bool,triang,dot],triang).
op_g(symmetrical,[bool,square,dot],square).
op_g(symmetrical,[bool,rectangle,dot],rectangle).
op_g(symmetrical,[bool,parallelogram,dot],parallelogram).
op_g(symmetrical,[bool,polygon,line],poygon).

%simetria axial
op_g(symmetrical,[bool,dot,line],dot).
op_g(symmetrical,[bool,line,line],line).
op_g(symmetrical,[bool,path,line],path).
op_g(symmetrical,[bool,right_triang,line],right_triang).
op_g(symmetrical,[bool,triang,line],triang).
op_g(symmetrical,[bool,square,line],square).
op_g(symmetrical,[bool,rectangle,line],rectangle).
op_g(symmetrical,[bool,parallelogram,line],parallelogram).
op_g(symmetrical,[bool,polygon,line],poygon).
