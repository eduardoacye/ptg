%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%			Operaciones sobre listas
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
append([],L,L).
append([X|L1],L2,[X|L3]) :- append(L1,L2,L3), !.

reverse([],[]).
reverse([H|T],L) :- reverse(T,Z), append(Z,[H],L), !.

not_member(_,[]).
not_member(E, [H|T]) :- 
		E \== H,
		not_member(E,T).

length_list([],0). 
length_list([H|T],N) :-
		length_list(T,M),
		N is M + 1, !.

empty_list(L) :-
		length_list(L,N),
		N == 0, !.

first([H|T],H) :- !.

last(LIST,L) :-
		reverse(LIST,R),
		first(R,L), !.

rotate_list([H|T],R) :- append(T,[H],R).

%filter_elment(E,L,F) filtra el elemento E de la lista L y regresa la lista F
filter_element(_,[],[]).
filter_element(E,[E|T],LIST) :-
			filter_element(E,T,LIST).

filter_element(E,[H|T],LIST) :-
			filter_element(E,T,R),
			append([H],R,LIST).


%return indexed object from list		
idx_object(0, [OBJECT|_], OBJECT).
idx_object(IDX, [_|OBJECTS_T], OBJECT) :-
			NEW_IDX is IDX - 1,
			idx_object(NEW_IDX, OBJECTS_T, OBJECT),
			!.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%			Funciones aritmeticas
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
max(X,Y,X) :- X >= Y.
max(X,Y,Y) :- Y >= X.
min(X,Y,X) :- X =< Y.
min(X,Y,Y) :- Y =< X.

abs(X,X) :- X >= 0.
abs(X,Y) :- X < 0, Y is - X.

	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%			Manipulacion de dots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%obtiene la lista de dots que definen a una figura
get_dots_fig(dot(true,X:Y),[dot(true,X:Y)]).
get_dots_fig(line(true,D1,D2),[D1,D2]).
get_dots_fig(path(true,DLIST),DLIST).
get_dots_fig(right_triang(true,D1,D2,D3),[D1,D2,D3]).
get_dots_fig(triang(true,D1,D2,D3),[D1,D2,D3]).
get_dots_fig(square(true,D1,D2,D3,D4),[D1,D2,D3,D4]).
get_dots_fig(rectangle(true,D1,D2,D3,D4),[D1,D2,D3,D4]).
get_dots_fig(polygon(true,DLIST),DLIST).


%Igualdad geometrica entre listas de dots con la misma longitud
%si no todos los pts. correspondientes de ambas listas son iguales, falla
equal_dots(DOTS1,DOTS2) :-
			length_list(DOTS1,N),
			length_list(DOTS2,N),
			positions_dots(DOTS1,POS1),
			positions_dots(DOTS2,POS2),
			equal_positions(POS1,POS2),!.

%Obtine la lista de posiciones de una lista de dots
positions_dots([],[]).
positions_dots([dot(true,X:Y)|T],POS) :-
			positions_dots(T,REST_POS),
			append([X:Y],REST_POS,POS),!.

%compara las posiciones correspondientes una a una
equal_positions([],[]).
equal_positions([H1|T1],[H2|T2]) :-
			equal_positions(T1,T2),
			equal_position(H1,H2),!.

equal_position(X1:Y1,X2:Y2) :-
			DIF1 is X1 - X2,
			DIF2 is Y1 - Y2,
			error(DIF1,0),
			error(DIF2,0),!.

%Checa que todos los dots (dot(B,X:Y) en una lista tengan posiciones diferentes
different_dots(DOTS) :-
			positions_dots(DOTS,POS),
			different_positions(POS),!.
			
%Checa que todas las posiciones en la lista sean diferentes o falla
different_positions([]).
different_positions([H|T]) :-
			%H es diferente de las posiciones en T
			pos_not_in_list(H,T),
			different_positions(T),!.

pos_not_in_list(_,[]).
pos_not_in_list(POS,[H|T]) :-
			match_points(B,POS,H),
			B = false,
			pos_not_in_list(POS,T),!.

%Shift lista de puntos hasta que un pto. en lista coincida con pto. de ref.
align_list_to_dot(REFDOT,[H|T],_,[H|T]) :-
			equal_dots([REFDOT],[H]),!.

align_list_to_dot(REFDOT,LIST,SHIFTS,ALIGNED) :-
			SHIFTS > 0,
			rotate_list(LIST,RLIST),
			N is SHIFTS - 1,
			align_list_to_dot(REFDOT,RLIST,N,ALIGNED),!.
			

%Rutinas auxiliares para checar la interseccion entre 2 poligonos
%construye la secuencia de lineas que definen a un poligono

%si dos vertices de las figuras estan a una distancia menor
%que el delta-error, el vertice de la fig 2 es realmente el de la Fig. 1
normalization_error(Fig1,Fig2,FigNor) :-
			 get_dots_fig(Fig1,Dots1),
			 get_dots_fig(Fig2,Dots2),
			 normalize_dots(Dots1,Dots2,DotsNor),
			 Fig2 =.. [OP|_],
			 FigNor =.. [OP|[true|DotsNor]].

normalize_dots([],D,D).
normalize_dots([HD|TD],Dots2,NewDots2) :-
			compare_dot_list(HD,Dots2,DotsNext),
			normalize_dots(TD,DotsNext,NewDots2).

compare_dot_list(_,[],[]).
compare_dot_list(dot(true,P1),[dot(true,P2)|TDots2],NewDots2) :-
			equal_position(P1,P2),
			compare_dot_list(dot(true,P1),TDots2,RestDots),
			append([dot(true,P1)],RestDots,NewDots2).

compare_dot_list(D1,[HD2|TDots2],NewDots2) :-
			compare_dot_list(D1,TDots2,RestDots),
			append([HD2],RestDots,NewDots2).

%rutinas para construir las lineas que forman a un poligono
get_lines_polygon(FIG,LINES) :-
			 eval_gterm(is_polygon(true,FIG),true),
			 get_dots_fig(FIG,Dots),
			 build_lines_polygon(Dots,LINES).

build_lines_polygon(Dots,Lines) :-
			first(Dots,First),
			reverse(Dots,ReverseList),
			first(ReverseList,Last),
			build_lines_fig(Dots,FirstLines),

			%include last line to close the polygon
			append(FirstLines,[line(true,Last,First)],Lines).

build_lines_fig([X,Y],[line(true,X,Y)]).
build_lines_fig([X|Rest],Lines) :-
			first(Rest,Y),
			build_lines_fig(Rest,RestLines),
			append([line(true,X,Y)],RestLines,Lines).
