%% -*- mode: prolog -*-

:- style_check(-singleton).
:- style_check(-discontiguous).

:- setenv('PTG', '.').
:- setenv('GEOMETRY', './geometry').
:- setenv('HETGEN_EVAL', './het_reasoning').
:- consult('./ptg').
:- consult('./test_eval').
:- test_eval.
