f :- reconsult('pitagoras/select_focus').

%Rutina que sugiere el el esquema de accion y el foco de atencion 
%para la siguiente accion en base al PACIENTE (i.e. el objeto
%modificado por el esquema de accion anterior), y el diagrama actual.
%
%La rutina select_focus tiene los siguientes argumentos:
%	   1. El diagrama actual
%	   2. El objeto modificado (paciente) por el ultimo esquema de accion
%	   3. El nuevo (sugerido) esquema de accion
%	   4. El foco para la aplicacion del nuevo esquema de accion


%regresa sch. de accion aplicables con sus focos potenciales
focus_heuristics(Current_State, In_Focus, Suggested_Actions) :-

	  get_right_triangs(Current_State, RTs),
	  length_list(RTs, NumObjects),
	  NumObjects > 1,

	  select_focus(RTs, NumObjects, In_Focus, act_sch_3, Foci_3),
	  get_list_act_foci(act_sch_3,Foci_3,Act_3),

%print('Foci 3: '),print(Foci_3),nl,nl,

	  select_focus(RTs, NumObjects, In_Focus, act_sch_2, Foci_2),
	  get_list_act_foci(act_sch_2,Foci_2,Act_2),

%print('Foci 2: '),print(Foci_2),nl,nl,

	  select_focus(RTs, NumObjects, In_Focus, act_sch_1, Foci_1),
	  get_list_act_foci(act_sch_1,Foci_1,Act_1),

%print('Foci 1: '),print(Foci_1),nl,nl,

	  append(Act_3, Act_2, Act_32),
	  append(Act_32, Act_1, Suggested_Actions).

%Start Pythagoras generation process
focus_heuristics(Current_State, In_Focus, [act_sch_2:[In_Focus]]) :-
	  get_right_triangs(Current_State, RTs),
	  length_list(RTs, N),
	  N == 1.

%Select act. sch. for the Theorem of the sum of the odds
focus_heuristics(Current_State, In_Focus, [act_sch_4:[In_Focus]]) :-
	 context_action_scheme_4(Current_State, In_Focus, SQ, CF).

get_list_act_foci(ActSch,[],[]).
get_list_act_foci(ActSch,Foci,[ActSch:Foci]).

%Primera Heuristica: estructura "holistica"
%selecciona action_scheme_3 & el foco es el arreglo de 4 RT formando
%un cuadrado sobre la hipotenusa si este arreglo de RT esta presente
%el diagrama si el Paciente no es el cuadrado hoyo 
%para prevenir que el cuadrado se aplique despues de la reinterpretacion

select_focus(RTs, NumObjects, Previous_Patient, act_sch_3, Foci) :-
	
	%there more be at least 4 RTs
	NumObjects >= 4,

	%Get Model Act. Sch. 3 (4 assigments in Model)
	combinations([RT1,RT2,RT3,RT4], RTs, Assigments),

	%Find combinations of RT's that satisfy action_scheme_3
	try_assigments(3, Assigments, RTs, Previous_Patient, Foci).

%Segunda Heuristica: Si hay un contexto que satisface la configuracion de salida del
%esquema de accion 2 (que es la configuracion de entrada el esquema 1), 
%y el Previous_Patient es es uno de los RTs en dicha configuracion, 
%se selecciona el esquema de accion 2, y el nuevo foco es el Previous_Patient

select_focus(RTs, NumObjects, Previous_Patient, act_sch_2, Foci) :-
	
	%there must be at least 2 RTs
	NumObjects >= 2,

	%Get Model Act. Sch. 2 (2 assigments in Model)
	combinations([RT0,RT1], RTs, Assigments),

	%Find combinations of RT's that satisfy action_scheme_2
	try_assigments(2, Assigments, _, Previous_Patient, Foci).

%Tercera Heuristica: Si hay una configuracion que satisface el contexto
%del primer esquema de accion, y ninguno de los RTs de la configuracion
%es el Previous_Patient (i.e. la configuracion no se modifico por el 
%esquema anterio, los dos RTs de la configuracion son focos potenciales
%y se incluyen en la lista de foci

select_focus(RTs, NumObjects, Previous_Patient, act_sch_1, Foci) :-

	%there must be at least 2 RTs
	NumObjects >= 2,

	%Get Model Act. Sch. 2 (2 assigments in Model)
	combinations([RT0,RT1], RTs, Assigments),

	%Find combinations of RT's that satisfy action_scheme_3
	try_assigments(1, Assigments, RTs, Previous_Patient, Foci).

select_focus(_, _, _, _, []).

%%%%%%%%%%%%%%% Rutinas de verificacion de modelos para esquemas %%%%%%%%%%%%%%%%%%%


try_assigments(1, Assigments, _, PPatient, Foci) :-

	%verifica los TR's satisfacen el contexto del esquema 2
	context_scheme_1(Assigments, Models),

	%Para cada modelo verifica si foci disponibles
	foci_scheme_1(Models, PPatient, Foci).

context_scheme_1([], []).
context_scheme_1([[X:RT0,Y:RT1]|T], Models) :-

	%verifica los TR's satisfacen el contexto del esquema 1
	%que es tambien el contexto de salida del esquema 2
	eval_gterm(aligned_AB_NCRV(true,RT0,RT1),true),
	context_scheme_1(T, Rest_Models),
	append([[X:RT0,Y:RT1]],Rest_Models, Models).

context_scheme_1([_|T], Models) :-
	context_scheme_1(T, Models).

foci_scheme_1([], _, []).
foci_scheme_1([[X:RT0,Y:RT1]|T], PPatient, Foci) :-

	%verifica que NINGUNO  de los dos triangulos sea el PPatient
	eval_gterm(eq(true, RT0, PPatient), false),
	eval_gterm(eq(true, RT1, PPatient), false),

	foci_scheme_1(T, PPatient, Rest_Foci),
	add_focus_list([RT0,RT1], Rest_Foci, Foci).

foci_scheme_1([_|T], PPatient, Foci) :-
	 foci_scheme_1(T, PPatient, Foci).

add_focus_list([], List, List).
add_focus_list([H|T], In_List, Out_List) :-
	 not_member(H, In_List),
	 add_focus_list(T, In_List, Next_List),
	 append([H], Next_List, Out_List).
add_focus_list([_|T], In_List, Out_List) :-
	 add_focus_list(T, In_List, Out_List).

try_assigments(2, Assigments, _, PPatient, Foci) :-
	context_scheme_1(Assigments, Models),
	foci_scheme_2(Models, PPatient, Foci).

foci_scheme_2([], _, []).
foci_scheme_2([[X:RT0,Y:RT1]|T], PPatient, Foci) :-

	%verifica que uno de los dos triangulos sea el Potential_Focus
	(eval_gterm(eq(true, RT0, PPatient), true);
	 eval_gterm(eq(true, RT1, PPatient), true)),

	 foci_scheme_2(T, PPatient, Rest_Foci),
	 append([PPatient], Rest_Foci, Foci).

foci_scheme_2([_|T], PPatient, Foci) :-
	 foci_scheme_2(T, PPatient, Foci).


try_assigments(3, Assigments, RTs, PPatient, Foci) :-

	context_scheme_3(RTs, Assigments, Models),
	foci_scheme_3(Models, PPatient, Foci).

context_scheme_3(_, [], []).
context_scheme_3(RTs, [[X:RT0,Y:RT1,Z:RT2,W:RT3]|T], Models) :-

	%verifica los TR's satisfacen el contexto del esquema 3
	context_action_scheme_3(RTs, [RT0, RT1, RT2, RT3], true),

	context_scheme_3(RTs, T, Rest_Models),
	append([[X:RT0,Y:RT1,Z:RT2,W:RT3]], Rest_Models, Models).

context_scheme_3(RTs, [_|T], Models) :-
	context_scheme_3(RTs, T, Models).

foci_scheme_3([], _, []).
foci_scheme_3([[X:RT0,Y:RT1,Z:RT2,W:RT3]|T], PPatient, Foci) :-

	%verifica que el cuadrado "hoyo" no sea el PPatient
	get_right_vertices([RT0, RT1, RT2, RT3], Right_Vertices),
	Square  =.. [square|[true|Right_Vertices]],
	eval_gterm(eq(true, Square, PPatient), false),

	foci_scheme_3(T, PPatient, Rest_Foci),
	append([[RT0, RT1, RT2, RT3]], Rest_Foci, Foci).

foci_scheme_3([_|T], PPatient, Foci) :-
	 foci_scheme_3(T, PPatient, Foci).
