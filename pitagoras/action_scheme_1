%% -*- mode: prolog -*-

%% Rota un triangulo recto Free sobre un TR Focus
action_scheme_1(In_Diagram, Focus, Out_Diagram, New_Focus) :-

    %% checa que le contexto del eschema se satisfaga por
    %% el diagrama y el focus
    context_action_scheme_1(In_Diagram, Focus, Patient, Pivot, Ang_Rot),

    %% realiza la rotacion del Paciente en relacion al Pivot por
    %% Ang_Rotation
    eval_gterm(rotate(true,Patient,Ang_Rot,Pivot),New_Focus),!,

    %% verifica que la rotacion del triangulo Free NO produce
    %% que este se intersecte con otro objeto en el diagram
    free_Space_in_Diagram(New_Focus, In_Diagram),

    %% Modifica diagrama y sugiere el nuevo foco
    substitute_geo_object(Patient, New_Focus, In_Diagram, Out_Diagram).

action_scheme_1(In_Diagram, Focus, Out_Diagram, New_focus) :-
    print('Diagram and Focus do not satisfy the context! '),nl.

context_action_scheme_1(In_Diagram, Focus, Patient, Pivot, Ang_Rotation) :-

    %% checa que foco este en el diagrama
    focus_in_diagram(Focus, In_Diagram, true),

    %% encuentra un TR Free que satisfaga el contexto
    %% del esquema en relacion al Focus
    %% Puede haber uno o dos
    get_patient_TR(Focus, In_Diagram, Patient_List),

    %% selecciona Paciente de la lista
    select_Patient(Patient_List, Patient),

    %% encuentra el pivote para la rotacion
    eval_gterm(vertex_aligned_AB_NCRV(true,Focus,Patient),Pivot),

    %% define el angulo para la rotacion (positivo o negativo)
    larger_side(Focus, LS),
    larger_side(Patient, LSF),

    %% define angulo base
    pi(PI),
    A90 is PI/2,

    ANG270 is A90 * 3,

    %% identifica direccion de rotacion
    dir_rotation_ActSch1(Focus, LS, Patient, LSF, ANG270, Ang_Rotation).

%% Verifica que exista un TR Free que satisfaga el contexto del esquema
%% con respecto al Focus
get_patient_TR(Focus, [], []).
get_patient_TR(Focus, [Patient|T], Patients_List) :-
    eval_gterm(aligned_AB_NCRV(true,Focus,Patient),true),
    get_patient_TR(Focus, T, Rest),
    append([Patient], Rest, Patients_List).
get_patient_TR(Focus, [_|T], Patients_List) :-
    get_patient_TR(Focus, T, Patients_List).

%% si solo hay un solo elemento es el Paciente
select_Patient([Patient|[]], Patient) :-
    print('The Pacient right triangle (only option left!) is: '),nl,
    print(Patient),nl.

%% en caso contrario pregunta al usuario por Paciente
select_Patient([H|T], Patient) :-
    print('The suggested patient is: '),nl,
    print(H),nl,
    print('Accept? (yes/no) '),nl,
    read(Resp),
    check_answer_patient(Resp, H, T, Patient).

check_answer_patient(Resp, Patient, _, Patient) :-
    Resp == si; Resp == ok; Resp == yes.
check_answer_patient(Resp, _ , T, Patient) :-
    select_Patient(T, Patient).

%% Identifica la dir. de rotacion de acuerdo a las orientaciones de
%% Focus y Free

%% Caso 1
dir_rotation_ActSch1(Focus, LS, Free, LSF, ANG, ANG) :-
    orientation(Focus,LS,right,up),
    orientation(Free,LSF,down,right).

dir_rotation_ActSch1(Focus, LS, Free, LSF, ANG, ANGROT) :-
    orientation(Focus,LS,right,up),
    orientation(Free,LSF,up,left),
    ANGROT is - ANG.

%% Caso 2
dir_rotation_ActSch1(Focus, LS, Free, LSF, ANG, ANG) :-
    orientation(Focus,LS,right,down),
    orientation(Free,LSF,down,left).

dir_rotation_ActSch1(Focus, LS, Free, LSF, ANG, ANGROT) :-
    orientation(Focus,LS,right,down),
    orientation(Free,LSF,up,right),
    ANGROT is - ANG.

%% Caso 3
dir_rotation_ActSch1(Focus, LS, Free, LSF, ANG, ANG) :-
    orientation(Focus,LS,left,down),
    orientation(Free,LSF,up,left).

dir_rotation_ActSch1(Focus, LS, Free, LSF, ANG, ANGROT) :-
    orientation(Focus,LS,left,down),
    orientation(Free,LSF,down,right),
    ANGROT is - ANG.

%% Caso 4
dir_rotation_ActSch1(Focus, LS, Free, LSF, ANG, ANG) :-
    orientation(Focus,LS,left,up),
    orientation(Free,LSF,up,right).

dir_rotation_ActSch1(Focus, LS, Free, LSF, ANG, ANGROT) :-
    orientation(Focus,LS,left,up),
    orientation(Free,LSF,down,left),
    ANGROT is - ANG.

%% Caso 5
dir_rotation_ActSch1(Focus, LS, Free, LSF, ANG, ANG) :-
    orientation(Focus,LS,up,right),
    orientation(Free,LSF,right,down).

dir_rotation_ActSch1(Focus, LS, Free, LSF, ANG, ANGROT) :-
    orientation(Focus,LS,up,right),
    orientation(Free,LSF,left,up),
    ANGROT is - ANG.

%% Caso 6
dir_rotation_ActSch1(Focus, LS, Free, LSF, ANG, ANG) :-
    orientation(Focus,LS,up,left),
    orientation(Free,LSF,right,up).

dir_rotation_ActSch1(Focus, LS, Free, LSF, ANG, ANGROT) :-
    orientation(Focus,LS,up,left),
    orientation(Free,LSF,left,down),
    ANGROT is - ANG.

%% Caso 7
dir_rotation_ActSch1(Focus, LS, Free, LSF, ANG, ANG) :-
    orientation(Focus,LS,down,right),
    orientation(Free,LSF,left,down).

dir_rotation_ActSch1(Focus, LS, Free, LSF, ANG, ANGROT) :-
    orientation(Focus,LS,down,right),
    orientation(Free,LSF,up,right),
    ANGROT is - ANG.

%% Caso 8
dir_rotation_ActSch1(Focus, LS, Free, LSF, ANG, ANG) :-
    orientation(Focus,LS,down,left),
    orientation(Free,LSF,left,up).

dir_rotation_ActSch1(Focus, LS, Free, LSF, ANG, ANGROT) :-
    orientation(Focus,LS,down,left),
    orientation(Free,LSF,right,down),
    ANGROT is - ANG.
