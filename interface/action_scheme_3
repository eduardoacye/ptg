%% -*- mode: prolog -*-

%% Genera un cuadrado "fondo" en el contexto de 4 triangulos rectos
%% alineados de tal forma que sus hipotenusas forman un cuadrado "figura"
%% El nuevo cuadrado es el nuevo foco
action_scheme_3(In_Diagram, Foci, Out_Diagram, New_Focus) :-

    %% checa que le contexto del eschema se satisfaga por
    %% el diagrama y el focus
    context_action_scheme_3(In_Diagram, Foci, true),

    %% crea square con la lista de vertices rectos de los
    %% triangulos rectos en Current_State
    get_right_vertices(Foci, Right_Vertices),

    %% Crea "ground square" (nuevo foco)
    New_Focus =.. [square|[true|Right_Vertices]],!,

                  %% Verifica que el nuevo sq no se intersecte con el diagrama
                  free_Space_in_Diagram(New_Focus, In_Diagram),

                  %% agrega nuevo cuadrado en el diagrama
                  augment_geo_object(New_Focus, In_Diagram, Out_Diagram).

action_scheme_3(_,_,_,_) :-
    print('Diagram and Focus do not satisfy the context! '),nl.

%% checa configuracion geometrica de los 4 triangulos
context_action_scheme_3(In_Diagram, [TR1, TR2, TR3, TR4], true) :-

    %% verifica que los triangulos de Foci esten en diagrama
    %% checa que foco este en el diagrama
    foci_in_diagram([TR1, TR2, TR3, TR4], In_Diagram, true),!,

    %% checa que el foci satisfaga el contexto geometrico
    %% en el que las hipotenusas de los cuatro triangulos
    %% formen un cuadrado figura, y surja un cuadro hoyo de fondo
    (eval_gterm(aligned_AB_NCRV(true,TR1,TR2),true),
     eval_gterm(aligned_AB_NCRV(true,TR2,TR3),true),
     eval_gterm(aligned_AB_NCRV(true,TR3,TR4),true),
     eval_gterm(aligned_AB_NCRV(true,TR4,TR1),true)).

context_action_scheme_3(_, _, false).

get_right_vertices([],[]).
get_right_vertices([H|T],Right_Vertices) :-
    eval_gterm(right_vertex(true,H),Right_Vertex),
    get_right_vertices(T,Rest_Right_Vertices),
    append([Right_Vertex],Rest_Right_Vertices,Right_Vertices).
