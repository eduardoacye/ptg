
# Definici�n de frames principales
frame .init
canvas .drawing_zone -background blue

# Seleccionar el file de definiciones
frame .init_df
label .input_defs -text "Enter definitions file"
entry .file_name  -textvariable file_name
button .accept_df  -text "Load" -command { prolog load_defs_file($file_name)}
pack .input_defs .file_name .accept_df -in .init_df -side left


# Seleccionar el principio de conservacion aritm�tico (el geom�trico siempre es estructurado)
frame .init_cp
label .lcp -text  "Select Aritmethic Conservation Principle"

frame .marco_botones
radiobutton .global  	-text "global (Theorem of Pythagoras)" -variable kind_of_cp -value global
radiobutton .structured -text "structured (Sum of the odds)    " -variable kind_of_cp -value structured
pack .global .structured -in .marco_botones -side top

pack .lcp .marco_botones -in .init_cp -side top
			
# comportamiento de radiobuttons
.global 	config -command { lee_princ_cons $kind_of_cp}
.structured 	config -command { lee_princ_cons $kind_of_cp}

frame .marco_pcg
label .tit_pcg		-text "Princ. Cons. Area:"
label .pcg		-text "                      " -relief sunken -anchor nw -justify left
pack .tit_pcg .pcg -in .marco_pcg -side left

frame .marco_pca
label .tit_pca		-text "Princ. Cons. Arith:"
label .pca		-text "                      " -relief sunken -anchor nw -justify left
pack .tit_pca .pca -in .marco_pca -side left


#Definir geometria del frame de datos de incializacion 
pack .init_df .init_cp .marco_pcg .marco_pca -in .init -side left

proc lee_princ_cons { kind_of_cp } {
	 	prolog "test_get($kind_of_cp, A, B)"
		.pcg config -text $prolog_variables(A)
		.pca config -text $prolog_variables(B)
		}


# definir geometr�a de la ventana principal
pack .init -side top
pack .drawing_zone -side bottom -fill both -expand 1

