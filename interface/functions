%% -*- mode: prolog -*-

%% Rutinas para enumeracion de funciones finitas, y para
%% definicion de permutaciones y combinaciones
%% requiere:
%%     $GEOMETRY/utilities
%%     $GEOMETRY/sets

%%%%%%%%%% ENUMERACION DE FUNCIONES %%%%%%%%%%%%%%%%%%%

%% Convert decimal number N to OUT base  BASE for N > 0.
convert(0,_,[]).
convert(N, BASE, OUT) :-
    X is N // BASE,
    Y is N mod BASE,
    convert(X, BASE, S),
    append(S, [Y], OUT),
    !.

%% make function_index: make INDEX of N digits in  base  BASE from dec-number I
set_index(0, 0, _, []).
set_index(0, N, _, INDEX) :-
    J is N - 1,
    set_index(0, J, _, IN),
    append([0], IN, INDEX),
    !.

set_index(I, DIGITS, BASE, INDEX) :-
    convert(I, BASE, OUT),
    fill_zeros(DIGITS, OUT, INDEX),
    !.

fill_zeros(N, BARE_INDEX, FILLED) :-
    length(BARE_INDEX, L),
    TO_ADD is N - L,
    set_index(0, TO_ADD, _, OFFSET),
    append(OFFSET, BARE_INDEX, FILLED),
    !.

%%%%%%%%%% COMBINACIONES %%%%%%%%%%%%%%%%%%%

%% Encuentra la lista de combinaciones de M objetos tomados N a la vez
%% (i.e. assigments: lista de listas de pares  [X:A,Y:B,...])
%% tal que cada lista representa una combinacion de
%% M objetos (i.e. A, B...) tomados  N (las variables  X, Y,..) a la vez

combinations(Vars, Objects, Assigments) :-
    %% compute the number of functions from Vars to Objects (from 0)
    length_list(Vars, N),
    length_list(Objects, M),
    M >= N,
    combinations_idx(N, M, CombIndices),
    get_assigments(Vars, Objects, CombIndices, Assigments).

combinations(_,_,_) :-
    print('error en combinaciones: hay mas posiciones que objectos! '),nl.

combinations_idx(N, M, CombIndices) :-
    M >= N,
    Funcs is M**N,
    get_combinations_list(0, Funcs, N, M, [], CombIndices).

combinations_idx(_,_,CombIndices) :-
    print('error en combinaciones_idx: hay mas posiciones que objectos! '),nl.

get_combinations_list(Func_Num, Max, N, M, In_Comb, Out_Comb) :-

    Func_Num < Max,
    set_index(Func_Num, N, M, FuncIdx),
    add_combination(FuncIdx, In_Comb, Next_Comb),

    %% Next Func.
    Next_Func is Func_Num + 1,
    get_combinations_list(Next_Func, Max, N, M, Next_Comb, Out_Comb).

%% All functions have been considered
get_combinations_list(_, _, _, _, Comb, Comb).

add_combination(FuncIdx, In_Comb, Next_Comb) :-
    is_permutation(FuncIdx, true),
    new_combination(In_Comb, FuncIdx, Next_Comb).

%% FuncIdx no es una permutacion (i.e. ergo tampoco combinacion)
add_combination(_, C, C).

%% add combination in list if FuncIdx is not a permutation of a
%% combination already there
new_combination([], FuncIdx, [FuncIdx]).
new_combination([H|T], FuncIdx, [H|T]) :-
    permutation_of(FuncIdx, H).
new_combination([H|T], FuncIdx, Next_Comb) :-
    new_combination(T, FuncIdx, Rest_Comb),
    append([H],Rest_Comb,Next_Comb).

%% El indice de fucn. es una permutacion de Comb
permutation_of([],_).
permutation_of([H|T],Comb) :-
    member(H, Comb),
    permutation_of(T, Comb).

%%%%%%%%%% PERMUTACIONES %%%%%%%%%%%%%%%%%%%
permutations(Vars, Objects, Assigments) :-
    %% compute the number of functions from Vars to Objects (from 0)
    length_list(Vars, N),
    length_list(Objects, M),
    M >= N,
    permutations_idx(N, M, PerIndices),
    get_assigments(Vars, Objects, PerIndices, Assigments).

permutations(Vars, Objects, PerIndices) :-
    print('error en permutations: hay mas posiciones que objectos '),nl.

permutations_idx(N, M, PerIndices) :-
    M >= N,
    Funcs is M**N,
    get_permutations_list(0, Funcs, N, M, [], PerIndices).

permutations_idx(Vars, Objects, PerIndices) :-
    print('error en permutations_idx: hay mas posiciones que objectos '),nl.

get_permutations_list(Func_Num, Max, N, M, In_Per, Out_Per) :-

    Func_Num < Max,
    set_index(Func_Num, N, M, FuncIdx),
    add_permutation(FuncIdx, In_Per, Next_Per),

    %% Next Func.
    Next_Func is Func_Num + 1,
    get_permutations_list(Next_Func, Max, N, M, Next_Per, Out_Per).

%% All functions have been considered
get_permutations_list(_, _, _, _, Per, Per).

add_permutation(FuncIdx, In_Per, Next_Per) :-
    is_permutation(FuncIdx, true),
    append(In_Per, [FuncIdx], Next_Per).

%% si FuncIdx no es permutation se omite
add_permutation(_,P,P).

%% verifica que le indice la de la funcion corresponde a una permutacion
%% i.e. todos los miembros del domino tiene asignado un objeto diferente
%% en el rango
is_permutation([],true).
is_permutation([H|Rest],T) :-
    not_member(H, Rest),
    is_permutation(Rest,T).
is_permutation(_,false).

%%%%%%%%%%%%% Asigna Variables a Objectos segun FuncIdx %%%%%%%%
%% Parametros:
%%     Var: variables o posiciones
%%     Objects: Objetos asignados segun PerIndices
%%     Lista de Idx para asignar Var a Objectos

get_assigments(_, _, [], []).
get_assigments(Vars, Objects, [H_Idx|Rest_Idx], Assigments) :-
    func_assigment(H_Idx, Vars, Objects, Assigment),
    get_assigments(Vars, Objects, Rest_Idx, Rest_Assigments),
    append([Assigment], Rest_Assigments, Assigments).

func_assigment([], [], _, []).
func_assigment([H_Idx|Rest_Idx], [Var|RVars], Objects, Assigment) :-
    idx_object(H_Idx, Objects, Object),
    func_assigment(Rest_Idx, RVars, Objects, Rest_Assigments),
    append([Var:Object],Rest_Assigments, Assigment).
